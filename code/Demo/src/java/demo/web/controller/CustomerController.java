/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.web.controller;

import demo.web.viewModel.CustomerViewModel;
import jack.annotation.Param;
import jack.web.View;

/**
 *
 * @author Java
 */
public class CustomerController {
    public View index(){
        demo.web.viewModel.CustomerViewModel viewModel = new CustomerViewModel();
        return new View("/view/customer/index.jsp",viewModel);
    }
    
    public View list(@Param(name="code",nullAble = true) String code
            ,@Param(name="name",nullAble = true) String name){
        demo.web.viewModel.CustomerViewModel viewModel = new CustomerViewModel();
        
        return new View("/view/customer/list.jsp",viewModel);
    }
}
