/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.web.el;

/**
 *
 * @author Java
 */
public class ELDatePickerOpt extends ELOpt {
    public Integer minDate;
    public Integer maxDate;
    public String onChange;
    public String yearRange = "c-50:c+5";
    public void setClassAttr(){
        if (disable == true)
        {
            classAttr = classAttr + " disableDatePicker ";
        }
        if (isValidateEmpty == true)
        {
            classAttr = classAttr + " ui-require-field validate";
        }
    }
}
