/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jack.model;

/**
 *
 * @author Java
 */
public enum SqlCondition {
    or(1){
        @Override
        public String strValue(){
            return "or";
        }
    },
    and(2){
        @Override
        public String strValue(){
            return "and";
        }
    };
    private int value;
    public abstract String strValue();
    
    private SqlCondition(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
    
    public static SqlCondition fromValue(int value) {  
        for (SqlCondition my: SqlCondition.values()) {  
            if (my.value == value) {  
                return my;  
            }  
        }  
        return null;  
    }  
}
