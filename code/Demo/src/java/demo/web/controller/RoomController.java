/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.web.controller;

import demo.model.dataModel.Room;
import demo.web.viewModel.RoomViewModel;
import jack.annotation.*;
import jack.web.View;
import java.util.Date;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author Java
 */
@WebServlet(name = "Room", urlPatterns = {"/room"})
public class RoomController extends jack.web.Controller {
    public View index(@Param(name = "name") String name){
       demo.web.viewModel.RoomViewModel viewModel = new RoomViewModel();
       viewModel.name = name;
       return new View("/view/room/index.jsp",viewModel);
    }
    
    public View list(
            @Param(name = "roomid" ,nullAble=true) Integer roomid,
            @Param(name="price",nullAble = true) String price)
       {
       demo.web.viewModel.RoomViewModel viewModel = new RoomViewModel();

       //<editor-fold desc="Mock">
       for(int i=0;i<=10;i++){
           Room x = new Room();
           x.roomID = String.valueOf(i); 
           x.name = "name_" + String.valueOf(i);
           x.price = i;
           viewModel.dataList.add(x);
       }
       //</editor-fold>
       return new View("/view/room/list.jsp",viewModel);
    }
    
    
    public View detail(
            @Param(name = "roomid") int roomid
            ,@Param(name = "name") String name
            ,@Param(name = "bookDate",nullAble = true) Date bookDate    
    )
    {
       demo.web.viewModel.RoomViewModel viewModel = new RoomViewModel();
       //viewModel.num = num;
       viewModel.roomID = roomid;
       viewModel.name = name;
       viewModel.bookDate = bookDate;
       
       
       
       return new View("/view/room/detail.jsp",viewModel);
    }
}
