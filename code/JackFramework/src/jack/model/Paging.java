/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.model;

/**
 *
 * @author Java
 */
public class Paging {

    private int _TotalPage;

    public int getTotalPage() {
        if (TotalItem == 0 || PageSize == 0 || PageNo == 0) return 0;
        else return (int)Math.ceil((double)TotalItem / (double)PageSize);
    }

    public void setTotalPage(int TotalPage) {
        this.TotalPage = TotalPage;
    }

    public int getTotalItem() {
        return TotalItem;
    }

    public void setTotalItem(int TotalItem) {
        this.TotalItem = TotalItem;
    }

    public int getPageNo() {
        return PageNo;
    }

    public void setPageNo(int PageNo) {
        this.PageNo = PageNo;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize+1;
    }

    public int getPageIndex() {
        return PageNo - 1;
        
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }
    public int TotalPage;
    //private int _ItemFirst;
    //private int _ItemLast;
    /// <summary>
    /// จำนวนรายการทั้งหมด
    /// </summary>
    public int TotalItem;
    /// <summary>
    /// หน้าปัจจุบัน
    /// </summary>
    public int PageNo;

    /// <summary>
    /// จำนวนรายการต่อหน้า
    /// </summary>
    public int PageSize;

    private int PageIndex;
    
    public Paging(){
    }
    
    public Paging(Integer pageNo,Integer pageSizeFromConfig){
        //Initial Paging
        //Paging paging = new Paging();
        if (pageNo == null || pageNo == 0)
        {
            //Inital Current Page | กำหนดหน้าปัจจุบัน
            this.PageNo = 1;
        }
        else
        {
            this.PageNo = (int)pageNo;
        }
        //pageSize from System Config
        if(pageSizeFromConfig == null){
              this.PageSize = 30;
        }else{
            this.PageSize = (int)pageSizeFromConfig;
        }
    }
}
