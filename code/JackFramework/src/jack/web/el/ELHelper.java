/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.web.el;

import jack.web.el.OptBtn;
import com.google.gson.Gson;
import jack.lang.JDate;
import jack.lang.JString;
import jack.model.ActionType;
import jack.model.Permission;
import jack.resources.Alert;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author Java
 */
public class ELHelper {
    public static String jqBtn(OptBtn options){
        try{
            String result="";
            if(options.color != ELColorBtn.none){
                options.classAttr = options.classAttr + " " + options.color.strValue();
            }
            
            if (options.disable == false)
            {
                result = "<span id=\"" + options.idAttr + "\" style=\"" + options.styleAttr + "\" jClick=\"" + options.onClick + "\" jIcon=\"" + options.btnIcon.jqIcon() + "\" class=\"button ElBtn " + options.classAttr + "\"  " + options.otherAttr + " >" + options.caption + "</span>";
            }
            else
            {
                result= "<span id=\"" + options.idAttr + "\" style=\"" + options.styleAttr + "\" class=\"button ElBtn disable" + options.classAttr + "\"  " + options.otherAttr + " >" + options.caption + "</span>";
            }
            return result;
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String button2(OptBtn options){
        try{
            String result="";
            options.otherAttr +=  " title=\""+ options.title + "\" size=\""+options.size.strValue()+"\"";
            options.classAttr += " " + options.btnIcon.imgIcon();
            if(options.color != ELColorBtn.none){
                options.classAttr = options.classAttr + " " + options.color.strValue();
            }
            String classIcon = "iconBtn ui-icon";    
            if (options.disable == true)
            {
                options.classAttr += " disabled ";
                options.otherAttr = " disabled=\"disabled\" ";
                classIcon = "ui-icon-disable ui-iconDis";
            }
            classIcon = classIcon + options.size.icon();
            result = "<button type=\"button\" id=\"" + options.idAttr + "\" style=\"" + options.styleAttr + "\""
                        + " class=\"jbutton " + options.classAttr + "\"" + options.otherAttr + " >";
            result += "<div style=\"width:"+options.size.strValue()+"px;height:"+options.size.strValue()+"px;\" "
                    + "class=\""+classIcon+" "+ options.btnIcon.imgIcon() + "\">";  
            result += "</div>";  
            result += "<div>" + options.caption + "</div>";
            result += "</button>";
            return result;
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String button(OptBtn options)
    {
        try
        {
            if (!JString.IsNullOrEmty(options.title))
            {
                options.otherAttr += " title=\"" + options.title + "\" ";
            }
            if (JString.IsNullOrEmty(options.classAttr)) { options.classAttr = ""; } else { options.classAttr = " " + options.classAttr; }
            
            options.styleAttr = " position:relative; " + options.styleAttr;
            String iconSize = options.size.strValue();
            String classButtonIcon = options.btnIcon.imgIcon();
            String classButtonIconAndSize = classButtonIcon + iconSize;
            options.otherAttr += " size=\"" + iconSize + "\"";
            //<editor-fold desc="button class.">
            options.classAttr = " jButton elBtn " + classButtonIcon +" " + options.classAttr;
            //</editor-fold>
            
            //<editor-fold desc="visible class.">
            if (options.visible == false) {
                options.styleAttr = "display:none;" + options.styleAttr;
            }
            //</editor-fold>

            //<editor-fold desc="icon Class">        
            String iconClass = " elBtnIcon ui-btn-icon" + iconSize + " " + classButtonIconAndSize;
            //</editor-fold>

            //<editor-fold desc="elBtnFrame"> 
            String frameClass = "elBtnFrame";  
            //</editor-fold>
            
            String caption = "";
            if(!JString.IsNullOrEmty(options.caption)){
                caption = "<div class=\"btnCaption\"  style=\"padding:4px 5px 4px 2px;font-weight: bold;\">" + options.caption + "</div>";
            }

            if (options.disable == true)
            {
                options.classAttr += " disabled ";
                iconClass = " elBtnIcon ui-btn-icon-Dis" + iconSize + " " + classButtonIconAndSize;
                options.otherAttr += " disabled=\"disabled\" ";
            }
            else { 
                
            }

            String result ="";
            result = "<button type=\"button\" id=\"" + options.idAttr + "\" style=\"" + options.styleAttr + "\""
                            + " actionType=\""+options.actionType+"\""
                            + " class=\"" + options.classAttr + "\"" + options.otherAttr + " jClick=\"" + options.onClick + "\" title=\"" + options.title + "\" >";
            
            String contenner = "<div style=\"display:table;border-spacing:0px;border-collapse:separate;padding:0px;\" class=\"" + frameClass + "\" style=\"text-align:center;height:" + iconSize + "px;\" >"
                                    + "<div style=\"display:table-row\">"
                                        + "<div style=\"display: table-cell;vertical-align:middle\"><div class=\"" + iconClass + "\" style=\"\" ></div></div>";
                                if(!JString.IsNullOrEmty(caption)){
                                        contenner += "<div style=\"display:table-cell;vertical-align:middle\">" + caption + "</div>";
                                }
                                contenner += "</div>";
                               contenner += "</div>";
            result += contenner;
            result += "</button>";

            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String BtnCaption(Permission permission,OptBtn options){
        try{
            String result = "";
            if(JString.IsNullOrEmty(options.title)){
                options.title = options.caption;
            }
            options.disable = true;
            options.title = Alert.NoPermission.getString();
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String BtnPermission(Permission permission,OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.permission;
            options.size = ELBtnSize.px26;
            options.title = jack.resources.Button.permission.getString();
            if(JString.IsNullOrEmty(options.title)){
                options.title = "";
            }
            if(permission.isView == true){
                
            }else{
                options.disable = true;
                options.title = Alert.NoPermission.getString();
            }
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    
    public static String BtnInsert(Permission permission,OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.add;
            options.size = ELBtnSize.px26;
            options.title = jack.resources.Button.add.getString();
            if(JString.IsNullOrEmty(options.title)){
                options.title = "";
            }
            if(permission.isInsert == true){
                    
            }else{
                options.disable = true;
                options.title = Alert.NoPermission.getString();
            }
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String BtnDelete(Permission permission,OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.delete;
            options.size = ELBtnSize.px26;
            options.title = jack.resources.Button.delete.getString();
            if(permission.isDelete == true){
                    
            }else{
                options.disable = true;
                options.title = Alert.NoPermission.getString();
            }
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String BtnUpdateOrView(Permission permission,OptBtn options){
        try{
            String result = "";
            options.title = jack.resources.Button.update.getString();
            if(permission.isUpdate == true){
                options.btnIcon = ELBtnIcon.edit;
                options.size = ELBtnSize.px26;  
                options.actionType = ActionType.update;
            }else{
                if(permission.isView == true){
                    options.btnIcon = ELBtnIcon.view;
                    options.size = ELBtnSize.px26;
                    options.actionType = ActionType.view;
                }else{
                    options.btnIcon = ELBtnIcon.view;
                    options.disable = true;
                    options.title = Alert.NoPermission.getString();
                    options.actionType = ActionType.view;
                }
            }
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String BtnSave(OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.save;
            options.size = ELBtnSize.px26;
            options.title = jack.resources.Button.save.getString();
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public static String BtnSave(Permission permission,jack.model.ActionType actionType,OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.save;
            options.size = ELBtnSize.px26;
            if(permission == null){
                options.disable = true;
                options.title = Alert.NoPermission.getString();
            }else{
                if(JString.IsNullOrEmty(options.title)){
                    options.title = "";
                }
                if(actionType == ActionType.insert && permission.isInsert == true){

                }
                else if(actionType == ActionType.update && permission.isUpdate == true){

                }else{
                    options.disable = true;
                    options.title = Alert.NoPermission.getString();
                }
            }
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public static String BtnCancel(OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.cancel;
            options.title = jack.resources.Button.cancel.getString();
            options.size = ELBtnSize.px26;
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public static String BtnClose(OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.close;
            options.size = ELBtnSize.px26;
            options.title = jack.resources.Button.close.getString();
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String BtnSearch(OptBtn options){
        try{
            String result = "";
            options.btnIcon = ELBtnIcon.search;
            options.size = ELBtnSize.px26;
            options.title = jack.resources.Button.search.getString();
            result = ELHelper.button(options);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public static String imag(String thumbImage,String fullImage,String imageGroup,ELImgOpt options){
        try{
            String result="";
            UUID obj = UUID.randomUUID();
            result = "<a href=\"" + fullImage + "?dummy=" + obj.toString() + "\" rel=\"prettyPhoto[" + imageGroup + "]\" title=\"" + options.title + "\">";
            result += "<div class=\"ShowDetail_thumbImage ImageFrame\" style=\"cursor: pointer; float:left; margin:0px 1px 0px 1px;\">";
            result += "<img src=\"" + thumbImage + "?dummy=" + obj.toString() + "\" id=\"" + options.idAttr + "\" name=\"" + options.nameAttr
                + "\" class=\"" + options.classAttr + "\" style=\"" + options.styleAttr + "\" "
                + options.otherAttr
                + " onError=\"this.src='"+options.rootURL+"/Content/Images/Image_NotAvailables.png'\" "
                + " />";
            result += "</div></a>";
            return result;    
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String textBox(Integer value,ELTextboxOpt options){
        try{
            String result;
            if(value == null){
                result = "";
            }else{
                result = value.toString();
            }
            return result;    
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String textBox(String value,ELTextboxOpt options) throws Exception {
        try{
            String result="";
            ELHelper.SetUIValidate(options);
            ELHelper.SetCanEditToClassAttr(options);
            if (options.textAlign != null && options.textAlign != ELTextAlign.none)
            {
                options.classAttr += " " + options.textAlign.strValue();
            }
            String attrReadOnly = "";
            if (options.disable == true){
                attrReadOnly = " readOnly = \"readOnly\" ";
                options.classAttr += " input-readOnly "; 
            }else{
                if (options.isValidateEmpty == true)
                {
                    options.classAttr += " ui-require-field validate ";
                }
            }
            if(JString.IsNullOrEmty(value)){
                value="";
            }
            
            if(options.isLabel == true){
                options.styleAttr = "border:none;background-color:transparent;" + options.styleAttr;
                attrReadOnly = " readOnly = \"readOnly\" ";
                if(JString.IsNullOrEmty(options.title)){
                    options.title = value;
                }
            }
            
            result = "<input type=\"text\" id=\"" + options.idAttr + "\" name=\"" + options.nameAttr + "\" style=\"" 
                    + options.styleAttr + "\" class=\"  " + options.classAttr + "\" " + options.otherAttr 
                    + " title=\"" + options.title + "\" "
                    + " value=\"" + value + "\" "+attrReadOnly+"  " + options.strTabIndex() + "  />";
            return result;    
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String hidden(String value,ELHiddenOpt options) throws Exception {
        try{
            String result="";
            result = "<input type=\"hidden\" id=\"" + options.idAttr + "\" name=\"" + options.nameAttr + "\" style=\"" 
                    + options.styleAttr + "\" class=\"  " + options.classAttr + "\" " + options.otherAttr 
                    + " title=\"" + options.title + "\" "
                    + " value=\"" + value + "\" />";
            return result;    
        }catch(Exception e){
            throw e;
        }
    }
    
    
    public static String datePicker(Date value,ELDatePickerOpt options) throws Exception{
        try{
            String result;
            if(value == null){
                result ="";
            }else{
                //convert
                result = JDate.ConvertToStr(value);
            }
            result = ELHelper.datePicker(result, options);
            return result;  
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String datePicker(String value,ELDatePickerOpt options) throws Exception{
        try{
            String result="";
            if (JString.IsNullOrEmty(value))
            {
                value = "";
            }
            ELHelper.SetUIValidate(options);
            ELHelper.SetCanEditToClassAttr(options);
            if (options.isValidateEmpty == true)
            {
                options.classAttr += " ui-require-field validate ";
            }
            if(JString.IsNullOrEmty(value)){
                value="";
            }
            if (options == null) { options = new ELDatePickerOpt(); }
            result = "<input type=\"text\" id=\"" + options.idAttr + "\" name=\"" + options.nameAttr
                + "\" style=\"" + options.styleAttr + "\"  class=\" pickdate " + options.classAttr + "\" "
                + " mindate='" + (options.minDate == null ? "" : options.minDate.toString()) + "' "
                + " maxdate='" + (options.maxDate == null ? "" : options.maxDate.toString()) + "' "
                + " yearRange='" + options.yearRange + "' "
                + " OnChange='" + options.onChange + "' "
                + options.otherAttr + " value=\"" + value + "\" defaultval=\"" + value + "\" />";
            return result;  
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String numeric(Integer value,ELTextboxOpt options){
        try{
            String result="";
            return result;    
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String label(String value,ELLabelOpt options){
        try{
            String result;
            if (options.textAlign != null && options.textAlign != ELTextAlign.none)
            {
                options.classAttr = options.classAttr + " " + options.textAlign.strValue();
            }
            
            result = "<label for=\"" + options.forAttr + "\" style=\"display:inline-block;" 
                    + options.styleAttr + "\" class=\"" + options.classAttr + "\" " + options.otherAttr 
                    + ">" + value + "</label>";
            return result;    
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String select(String value,ELSelectOpt options,jack.web.Json data) throws Exception{
        try{
            
            String result="";
            //<editor-fold desc="convert json to string">
            Gson gson = new Gson();
            String sJSON = gson.toJson(data.getObj());
            //</editor-fold>
            String validateEmpty = "";
            ELHelper.SetUIValidate(options);
            ELHelper.SetCanEditToClassAttr(options);
            if (options.isValidateEmpty == true)
            {
                validateEmpty = "ui-require-field validate";
            }
            if(JString.IsNullOrEmty(value)){
                value="";
            }
            result = "<select id=\"" + options.idAttr + "\" name=\"" + options.nameAttr + "\" style=\"" + options.styleAttr + "\""
                + " class=\" isGetDataFromEl " + validateEmpty + " " + options.classAttr + "\" " + options.otherAttr
                + " value1=\"" + value + "\" hasSelectListData=\"true\""
                + " onChange=\"" + options.onChange + "\" callBack=\"" + options.callBack + "\" DefaultVal=\"" + value + "\" />";
            result += "</select>";
            result += "<input type='textbox' class='selectListData' value='" + sJSON + "' style='display:none'>";
            return result;    
        }catch(Exception e){
            throw e;
        }
    }

    public static String textArea(String value, ELTextAreaOpt option) throws Exception
    {
            try
            {
                if (option == null) { option = new ELTextAreaOpt(); }
                ELHelper.SetUIValidate(option);
                ELHelper.SetCanEditToClassAttr(option);
                String  validateEmpty = "";
                if (option.isValidateEmpty == true)
                {
                    validateEmpty = "ui-require-field validate ";
                }
                if (option.isLabel == true)
                {
                    option.styleAttr = "border:none;background-color:transparent;" + option.styleAttr;
                }
                String result = "<div class='el-TextArea-section'><div><textarea id=\"" + option.idAttr
                    + "\" style=\"" + option.styleAttr
                    + "\" name=\"" + option.nameAttr
                    + "\" class=\"  " + validateEmpty + option.classAttr
                    + "\" placeholder=\"" + option.placeholder
                    + "\" " + option.otherAttr + "  DefaultVal=\"" + value + "\"  >" + value + "</textarea></div></div>";
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
      }
    
    public static String checkBox(boolean Checked , String value, ELCheckBoxOpt options)
    {
            try
            {
                if (options == null) { options = new ELCheckBoxOpt(); }
                ELHelper.SetCanEditToClassAttr(options);
                String checkedEl = "";
                String result="";
                if (Checked == true)
                {
                    checkedEl = " checked=\"checked\" ";
                }
                if (true == options.disable)
                {
                    result = "<input type=\"checkbox\" id=\"" + options.idAttr + "\" name=\"" + options.nameAttr 
                                    + "\" class=\"checkbox " + options.classAttr + "\" " + options.otherAttr + " style=\"display:none\""
                                    + " value=\"true\"" + checkedEl + " />";
                    result += "<input type=\"checkbox\" id=\"" + options.idAttr + "_tempControl" 
                                    + "\" class=\"checkbox tempControl" + options.classAttr + "\" " + options.otherAttr 
                                    + " value=\"" + value + "\"" + checkedEl + " disabled  />";
                }
                else
                {
                    result = "<input type=\"checkbox\" id=\"" + options.idAttr + "\" name=\"" + options.nameAttr
                        + "\" class=\"checkbox " + options.classAttr + "\" " + options.otherAttr
                        + " value=\"" + value + "\"" + checkedEl + " />";
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
      }
    
    
    public static ELOpt SetUIValidate(ELOpt elOpt) throws Exception {
        try{
            if(elOpt.model !=null && !JString.IsNullOrEmty(elOpt.fieldName)){
                jack.model.FieldValidateModel   fieldValidateModel= jack.common.Validation.getFieldValidateModel(elOpt.model,elOpt.fieldName);
                if(fieldValidateModel != null){
                    if(fieldValidateModel.has_NotNullValidator != null &&  fieldValidateModel.has_NotNullValidator== true){
                        elOpt.isValidateEmpty = true;
                    }
                }
            }
            return elOpt;
        }catch(Exception ex){
            throw ex;
        }
    }
    
    private static ELOpt SetCanEditToClassAttr(ELOpt elOpt){
        try{
            
            if(elOpt.canEdit == false){
                return elOpt;
            }else{
                elOpt.classAttr = elOpt.classAttr + " canEdit ";
                return elOpt;
            }
        }catch(Exception ex){
            throw ex;
        }
    }
    
}
