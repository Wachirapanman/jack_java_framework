/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.web.el;

/**
 *
 * @author Java
 */
public enum ELBtnIcon {
    none(1){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "";
        }
    },
    add(2){
        @Override
        public String jqIcon(){
            return "ui-icon-plus";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-add";
        }
    },
    edit(3){
        @Override
        public String jqIcon(){
            return "ui-icon-edit";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-edit";
        }
    },
    delete(4){
        @Override
        public String jqIcon(){
            return "ui-icon-trash";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-delete";
        }
    },
    view(5){
        @Override
        public String jqIcon(){
            return "ui-icon-view";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-view";
        }
    },
    save(6){
        @Override
        public String jqIcon(){
            return "ui-icon-disk";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-save";
        }
    },
    cancel(7){
        @Override
        public String jqIcon(){
            return "ui-icon-cancel";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-cancel";
        }
    },
    search(8){
        @Override
        public String jqIcon(){
            return "ui-icon-search";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-search";
        }
    },
    print(9){
        @Override
        public String jqIcon(){
            return "ui-icon-print";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-print";
        }
    },
    close(10){
        @Override
        public String jqIcon(){
            return "ui-icon-closethick";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-close";
        }
    },
    user(11){
        @Override
        public String jqIcon(){
            return "ui-icon-";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-user";
        }
    },
    download(12){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-download";
        }
    },
    upload(13){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-upload";
        }
    },
    config(14){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-config";
        }
    },
    email(15){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-email";
        }
    },
    note(16){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-note";
        }
    },
    folder(17){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-folder";
        }
    },
    history(18){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-history";
        }
    },
    browse(19){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-browse";
        }
    },
    camera(20){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-camera";
        }
    },
    select(21){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-select";
        }
    },
    permission(22){
        @Override
        public String jqIcon(){
            return "";
        }
        @Override
        public String imgIcon(){
            return "ui-btn-icon-permission";
        }
    }
    ;
    
    private int value;
    public abstract String jqIcon();
    public abstract String imgIcon();
    
    private ELBtnIcon(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
    
    public static ELBtnIcon fromValue(int value) {  
        for (ELBtnIcon my: ELBtnIcon.values()) {  
            if (my.value == value) {  
                return my;  
            }  
        }  
        return null;  
    }  
}
