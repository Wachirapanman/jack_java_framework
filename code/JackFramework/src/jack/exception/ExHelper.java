/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.exception;

import jack.web.Json;
import jack.web.JsonResult;
import java.util.ArrayList;

/**
 *
 * @author Java
 */
public class ExHelper {
    public JsonResult ConvertToJsonResult(Exception ex){
        JsonResult jsonResult = new JsonResult();
        jsonResult.success = false;
        jsonResult.errorContainer = new ErrorContainer();
        jsonResult.errorContainer.errorList = new ArrayList<>();
        ErrorModel errorModel = new ErrorModel();
        errorModel.errorKey = "";
        errorModel.errorMsg = ex.getMessage();
        jsonResult.errorContainer.errorList.add(errorModel);
        return jsonResult;
    }
}
