/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.exception;

/**
 *
 * @author Java
 */
public enum ErrorType {
    error(1){
        @Override
        public String caption() {
            return "error";
        }
    },
    warning(2){
        @Override
        public String caption() {
            return "warning";
        }
    },
    info(3){
        @Override
        public String caption() {
            return "info";
        }
    };
    
    private int value;
    public abstract String caption();
    private ErrorType(int value) {
        this.value = value;
    }
}
