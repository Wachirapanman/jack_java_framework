/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.resources;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Java
 */
public enum Button {
    add,
    update,
    delete,
    view,
    save,
    cancel,
    close,
    search,
    permission
    ;
    public String getString() {
        try{
            ResourceBundle bundle =
            ResourceBundle.getBundle("jamjun.infra.resources.button", Locale.getDefault());
            return bundle.getString(this.toString());
        }catch(Exception ex){
            return ex.getMessage();
        }
    }
    public String getString(Locale locale) {
         try{
            ResourceBundle bundle =
            ResourceBundle.getBundle("jamjun.infra.resources.button", locale);
            return bundle.getString(this.toString());
        }catch(Exception ex){
            return ex.getMessage();
        }
    }
}
