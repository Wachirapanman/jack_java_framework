/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.model;

import jack.annotation.validate.NotNullValidator;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
/**
 *
 * @author Java
 */
public class UserSession {
    @NotNullValidator(ResourceName = "CannotBeEmpty",Tag = "sessionID",BaseResource = "")
    public String sessionID;
    public String userID;
    public String userRoleID;
    public String ipAddress;
    public String userAgent;
    public Locale locale;
    public String strLocale;
    public TimeZone timeZone;
    public String strTimeZone;
    public Date createDate;
    public Boolean isActive;


    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getStrLocale() {
        return strLocale;
    }

    public void setStrLocale(String strLocale) {
        this.strLocale = strLocale;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public String getStrTimeZone() {
        return strTimeZone;
    }

    public void setStrTimeZone(String strTimeZone) {
        this.strTimeZone = strTimeZone;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
