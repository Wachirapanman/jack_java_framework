<%-- 
    Document   : index
    Created on : Dec 20, 2017, 12:44:13 AM
    Author     : Java
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="/view/share/header.jsp" />
        <jsp:useBean id="Model" class="demo.web.viewModel.CustomerViewModel"  scope="session"/> 
        <title></title>
    </head>
    <body>
        <h1>Customer</h1>
        
        <div id="Cust_list"></div>
    </body>
</html>
<script type="text/javascript">
    var customer_var = {
        
    };
    var customer_service = {
        loadList:function(){
            $("#Cust_list").load("router?[url]=customer/list", function() {
                
            });
        }
    };
    
    var customer_main = {
        load:function (){
            customer_service.loadList();
            this.assignEvent();
        },
        assignEvent:function(){

        }
    };
    $(document).ready(function() {
        customer_main.load();
    });

</script>
   