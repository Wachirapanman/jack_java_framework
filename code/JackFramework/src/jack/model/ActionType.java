/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.model;

/**
 *
 * @author Java
 */
public enum ActionType {
    view(1){
        @Override
        public String caption(){
            return "view";
        }
    },
    insert(2){
        @Override
        public String caption(){
            return "insert";
        }
    },
    update(3){
        @Override
        public String caption(){
            return "update";
        }
    },
    delete(4){
        @Override
        public String caption(){
            return "delete";
        }
    },
    approve(4){
        @Override
        public String caption(){
            return "approve";
        }
    };
    
    private int value;
    public abstract String caption();
    
    private ActionType(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
    
    public static ActionType fromValue(int value) {  
        for (ActionType my: ActionType.values()) {  
            if (my.value == value) {  
                return my;  
            }  
        }  
        return null;  
    }  
}
