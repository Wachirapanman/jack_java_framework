/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) //กำหนดว่าใช้เวลา Runtime
//@Target({ElementType.TYPE_PARAMETER}) //กำหนดว่าใช้กับ parameter

public @interface JController {
    String name();
}
