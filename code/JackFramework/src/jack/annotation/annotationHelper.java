/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jack.annotation;

import java.lang.reflect.Field;

/**
 *
 * @author Java
 */
public class annotationHelper {
    public static <T> String GetColumnName(T model,String fieldName) throws NoSuchFieldException{
        String columnName="";
         try{
            Class klazz = model.getClass();
            Field filed;
            try{
                 filed = klazz.getField(fieldName);
            }catch(NoSuchFieldException | SecurityException e){
               throw e;
            }
            Column column = null;
            try{
               column = filed.getAnnotation(Column.class);
            }catch(Exception e){
                //not to do;
            }
            if (column != null){
                columnName = column.name();
            }
            return columnName;
        }catch(NoSuchFieldException | SecurityException ex){
            throw ex;
        }
    }
}
