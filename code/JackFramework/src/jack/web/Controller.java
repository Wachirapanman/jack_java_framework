/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.web;

import jack.lang.JString;
import com.google.gson.Gson;
import jack.exception.ErrorContainer;
import jack.exception.ErrorModel;
import jack.exception.JException;
import jack.lang.JDate;
import jack.model.JackXML;
import static jack.web.CHelper.MappingRequestToObj;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;



/**
 *
 * @author Java
 */
@MultipartConfig
public class Controller extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        try{
            doDespatch(request,response);
        }catch(IOException | ServletException e){
            PrintWriter out = response.getWriter();
            out.print(e.toString());
        } catch (Exception ex) {
            this.DisplayError(ex);
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            request.setCharacterEncoding("UTF8"); 
            doDespatch(request,response);
        }catch(IOException | ServletException e){
            PrintWriter out = response.getWriter();
            out.print(e.toString());
        } catch (Exception ex) {
            this.DisplayError(ex);
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

    private void doDespatch(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException, Exception {
        ErrorContainer errorContainer = new ErrorContainer();
        ErrorModel errorModel = new ErrorModel();
        
        //response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            JSession.Request = request;
            JSession.Response = response;
            //JSession.jackXML = this.GetJackXML(request);
            String controllerName;
            String actionName;

            actionName = request.getParameter("[action]");
            if(JString.IsNullOrEmty(actionName)){
                errorModel.errorMsg = "Error:404 No [action]";
                errorContainer.errorList.add(errorModel);
                throw new JException(errorContainer);
            }


            controllerName = this.getClass().getName();
            Class clazz = Class.forName(controllerName);
            Object clazzObj = clazz.newInstance();
            Result result = null;
            List<Object> paramObjList = new ArrayList<>();
            Method actionMethod = this.GetAction(clazz, actionName);
            Parameter[] parameters = actionMethod.getParameters();
            this.MappingParameter(parameters, paramObjList);
            result = (Result) actionMethod.invoke(clazzObj,paramObjList.toArray()); 
            if(result == null){
                throw new Exception("result not null");
            }
            this.Render(result, request, response);
        }
        catch(JException je) {
            DisplayError(je);
        }
        catch(IOException | ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException | ServletException e){
            //e.printStackTrace();
            DisplayError(e);
        }
    }
    
    
    private void Render(Result result,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        if(null != result.getResultType())switch (result.getResultType()) {
            case view:
                response.setContentType("text/html;charset=UTF-8");
                View v;
                v =(View) result;
                jack.web.CHelper.View(v.getView_name(), request, response, v.getModel());
                break;
            case content:{
                response.setContentType("text/html;charset=UTF-8");
                Content c;
                c =(Content) result;
                PrintWriter out = response.getWriter();
                out.print(c.getContentString());
                    break;
                }
            case json:{
                response.setContentType("application/json;charset=UTF-8");
                PrintWriter out = response.getWriter();
                Json j;
                j =(Json) result;
                Gson gson = new Gson();
                String json = gson.toJson(j.getObj());
                out.write(json);
                    break;
                }
            case file:
                break;
            default:
                break;
        }
    }
    private Method GetAction(Class clazzController,String actionName) throws JException{
        Method action = null;
        Method[] declaredMethods = clazzController.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            if (declaredMethod.getName().equals(actionName)) {
                action = declaredMethod;
                break;
            }
            
        }
        if(action == null){
            ErrorContainer errorContainer = new ErrorContainer();
            ErrorModel errorModel = new ErrorModel();
            errorModel.errorMsg = "Incorrect Action";
            errorContainer.errorList.add(errorModel);
            throw new JException(errorContainer);
        }
        return action;
    }
    private List<Object> MappingParameter(Parameter[] parameters,List<Object> paramObjList) throws Exception{
        //<editor-fold desc="loop parameter">
        for (Parameter parameter : parameters) {
            Annotation[] annotations = parameter.getDeclaredAnnotations();
            Type paramType= parameter.getType();
            String paramClsName =paramType.getTypeName();
            Class paramCls;
            Enum paramEnum;
            Object paramObj =null;
            for(Annotation annotation : annotations){
                if(annotation instanceof jack.annotation.Param){
                    jack.annotation.Param paramAnnotation = (jack.annotation.Param) annotation;
                    try{
                        paramObj = JSession.getRequest().getParameter(paramAnnotation.name());
                    }catch(Exception e){
                        throw e;
                    }
                    boolean nullAble = paramAnnotation.nullAble();
                    String strParamValue = "";
                    if(paramObj != null){
                        strParamValue = paramObj.toString();
                    }
                    Object value = null;
                    if(parameter.getType().isPrimitive()){
                        //<editor-fold desc="mapping primitive type">
                        if(paramType.getTypeName().equals(int.class.getTypeName())){
                            if(JString.IsNullOrEmty(strParamValue)){
                                throw new Exception("Require Parameter Primitive not null:" + paramAnnotation.name());
                            }else{
                                value = Integer.parseInt(strParamValue);
                            }
                        }else if(paramType.getTypeName().equals(double.class.getTypeName())){
                            if(JString.IsNullOrEmty(strParamValue)){
                                throw new Exception("Require Parameter Primitive not null:" + paramAnnotation.name());
                            }else{
                                value = Double.parseDouble(strParamValue);
                            }
                        }
                        //</editor-fold>
                        paramObjList.add(value);
                    }else if(paramType instanceof Class && ((Class<?>)paramType).isEnum()){
                        paramCls = Class.forName(paramClsName);
                        if(paramType instanceof Class && ((Class<?>)paramType).isEnum()){
                            //Enum paramEnum;
                        }else{
                            paramObj = paramCls.newInstance();
                        }
                        //paramObj = GSession.getRequest().getParameter(paramAnnotation.name());
                        String valueOfEnum = "";
                        //<editor-fold desc="Get Enum of value string from httpRequest"> 
                        try{
                            valueOfEnum = JSession.getRequest().getParameter(paramAnnotation.name());
                        }catch(Exception e){
                            throw e;
                        }   
                        //</editor-fold>
                        if(!JString.IsNullOrEmty(valueOfEnum)){
                            paramEnum=Enum.valueOf(paramCls,valueOfEnum);
                        }else{
                            paramEnum=null;
                        }
                        paramObjList.add(paramEnum);
                    }else{
                        //<editor-fold desc="mapping Object Type">
                        if(paramType.getTypeName().equals(String.class.getTypeName())){
                            value = strParamValue;
                            paramObjList.add(value);
                        }
                        else if(paramType.getTypeName().equals(Integer.class.getTypeName())){
                            if(nullAble == false && JString.IsNullOrEmty(strParamValue)){
                                throw new Exception("Require Parameter Integer:" + paramAnnotation.name());
                            }
                            if(!JString.IsNullOrEmty(strParamValue)){
                                value = Integer.parseInt(strParamValue);
                            }
                            paramObjList.add(value);
                        }
                        else if(paramType.getTypeName().equals(Double.class.getTypeName())){
                            if(nullAble == false && JString.IsNullOrEmty(strParamValue)){
                                throw new Exception("Require Parameter Double:" + paramAnnotation.name());
                            }
                            if(!JString.IsNullOrEmty(strParamValue)){
                                value = Double.parseDouble(strParamValue);
                            }
                            paramObjList.add(value);
                        }
                        else if(paramType.getTypeName().equals(Float.class.getTypeName())){
                            if(nullAble == false && JString.IsNullOrEmty(strParamValue)){
                                throw new Exception("Require Parameter Float:" + paramAnnotation.name());
                            }
                            if(!JString.IsNullOrEmty(strParamValue)){
                                value = Float.parseFloat(strParamValue);
                            }
                            paramObjList.add(value);
                        }
                        else if(paramType.getTypeName().equals(Date.class.getTypeName())){
                            if(nullAble == false && JString.IsNullOrEmty(strParamValue)){
                                throw new Exception("Require Parameter Date:" + paramAnnotation.name());
                            }
                            if(!JString.IsNullOrEmty(strParamValue)){
                                try {
                                    value = JDate.StrConverToDateUTC(strParamValue);
                                } catch (IllegalAccessException ex) {
                                    Logger.getLogger(CHelper.class.getName()).log(Level.SEVERE, null, ex);
                                    throw ex;
                                }
                            }
                            paramObjList.add(value);
                        }
                        else if(paramType.getTypeName().equals(Boolean.class.getTypeName())){
                            if(JString.IsNullOrEmty(strParamValue)){
                                value = Boolean.FALSE;
                            }else{
                                value = Boolean.parseBoolean(strParamValue);
                            }
                            paramObjList.add(value);
                        }
                        //</editor-fold>
                        else{
                            //mapping data model
                            paramObj = MappingRequestToObj(paramObj,paramAnnotation.name(),JSession.getRequest());
                            paramObjList.add(paramObj);
                        }
                        
                        
                    }

                    System.out.println("name: " + paramAnnotation.name());
                }
            }
                
        }
        //end for (Parameter parameter : parameters) {
        //</editor-fold>
        return paramObjList;
    }
    private void DisplayError(JException e) throws IOException{
        PrintWriter out = JSession.Response.getWriter();
        ErrorModel errorModel = e.errorContainer.errorList.stream().findFirst().get();
        String msg = JString.IsNullOrEmty(errorModel.errorKey) ? "" : errorModel.errorKey + ":";
        msg += errorModel.errorMsg;
        out.print(msg);
    }
    private void DisplayError(Exception e) throws IOException{
        PrintWriter out = JSession.Response.getWriter();
        out.print("Error:" + e.toString());
    }
    private JackXML GetJackXML(HttpServletRequest request) throws ParserConfigurationException, SAXException, IOException{
        JackXML jackXMLConfig = new JackXML();
        String contextPath = request.getSession().getServletContext().getRealPath("/");
        File jackXML = new File(contextPath + "/jack.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(jackXML);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("package-controller");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                jackXMLConfig.package_controller = eElement.getAttribute("name");    
            }
        }
        return jackXMLConfig;
    }
    private void initConfig(HttpServletRequest request) throws Exception{
        //<editor-fold desc="set package-controller">
            //<editor-fold desc="check exiting">
            JSession jsession = null;
            try{
                jsession = jack.web.ManageSession.getJession(request);
            }catch(Exception e){
                if(jsession == null){
                    //set
                    jsession = new JSession();
                    //jsession = this.GetJackXML(request, jsession);
                    jack.web.ManageSession.setJession(request, jsession);
                }
            }
            //</editor-fold>
        //</editor-fold>
    }
    
    


    
    //------------- not use
    private Class GetControllerClass(String controllerName,HttpServletRequest request) throws ClassNotFoundException, Exception{
        String classControllerName = "";
        try{
            String first = controllerName.substring(0,1);
            first = first.toUpperCase();
            controllerName = first + controllerName.substring(1);
            classControllerName = JSession.jackXML.package_controller+"."+controllerName + "Controller";
            Class cls = Class.forName(classControllerName);
            return cls;
        }catch(ClassNotFoundException e){
            throw new Exception("please check jack.xml : package_controller or "+ classControllerName);
        }
    }
        private void doDespatch4(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException, Exception {
        ErrorContainer errorContainer = new ErrorContainer();
        ErrorModel errorModel = new ErrorModel();
        
        //response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            JSession.Request = request;
            JSession.Response = response;
            JSession.jackXML = this.GetJackXML(request);
                
            String path = request.getParameter("[url]");
            if(path == null){
                errorModel.errorMsg = "Error:404";
                errorContainer.errorList.add(errorModel);
                throw new JException(errorContainer);
            }
            String[] paths = path.split("/");
            //out.print(path + paths.length);    
            if(paths.length != 2 || paths.length == 0){
                errorModel.errorMsg = "Require Controller and Action";
                errorContainer.errorList.add(errorModel);
                throw new JException(errorContainer);
            }else{
                String controllerName = paths[0];
                String actionName = paths[1];
                Class clazz = this.GetControllerClass(controllerName,request);
                Object clazzObj = clazz.newInstance();
                Result result = null;
                List<Object> paramObjList = new ArrayList<>();
                Method actionMethod = this.GetAction(clazz, actionName);
                Parameter[] parameters = actionMethod.getParameters();
                this.MappingParameter(parameters, paramObjList);
                result = (Result) actionMethod.invoke(clazzObj,paramObjList.toArray()); 
                if(result == null){
                    throw new Exception("result not null");
                }
                this.Render(result, request, response);
            }
            
        }
        catch(JException je) {
            DisplayError(je);
        }
        catch(IOException | ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException | ServletException e){
            //e.printStackTrace();
            DisplayError(e);
        }
    }
    
    private List<RoutModel> getRout(HttpServletRequest request) throws IOException, Exception{
        try{
            String contextPath = request.getSession().getServletContext().getRealPath("/");
            File jackXML = new File(contextPath + "/jack.xml");
            List<RoutModel> routModelList = new ArrayList<>();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(jackXML);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("controller");
            
            for (int temp = 0; temp < nList.getLength(); temp++) {
                RoutModel routModel = new RoutModel();
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    routModel.controller_Name = eElement.getAttribute("name");
                    routModel.controller_Class = eElement.getAttribute("class");
                    
                    NodeList nLv2 = eElement.getElementsByTagName("action");
                    routModel.action = new ArrayList<>();
                    for (int tempLv2 = 0; tempLv2 < nLv2.getLength(); tempLv2++) {
                        Node nNodeLv2 = nLv2.item(tempLv2);
                        if (nNodeLv2.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElementLv2 = (Element) nNodeLv2;
                            RoutAction rouptAction = new RoutAction();
                            rouptAction.action_Name = eElementLv2.getAttribute("name");
                            routModel.action.add(rouptAction);
                        }
                    }
                    routModelList.add(routModel);
                }
            }
            return routModelList;
            
        }
        catch(IOException | ParserConfigurationException e)
        {
            //e.printStackTrace();
            throw new Exception(e.toString());
        }
    }
    
    private void doDespatch2(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException, Exception {
        try{
            String urlName = request.getPathInfo().substring(1);
            
            String controller;
            String action;
            List<RoutModel> routModelList;
            //<editor-fold desc="getURl controller and Action">
            String url = request.getParameter("[url]");
            String[] urls =  url.split("/");
            controller = urls[0];
            action = urls[1];
            if(JString.IsNullOrEmty(controller)){
                throw new Exception("No controller");
            }
            if(JString.IsNullOrEmty(action)){
                throw new Exception("No action");
            }
            // </editor-fold>
            
            //<editor-fold desc="Read Jack.XML">
            routModelList = this.getRout(request);

            // </editor-fold>
            
            //<editor-fold desc="find and check controller and action">
            RoutModel routModel;
            routModel = (RoutModel) routModelList.stream()
                    .filter(x->(x.controller_Name == null ? controller == null : x.controller_Name.equals(controller)))
                    .findFirst().orElse(null);
            if(routModel == null){
                throw new Exception("No URL in jack.xml");
            }
            
//            response.setContentType("text/html;charset=UTF-8");
//            PrintWriter out = response.getWriter();
//            out.print(routModel.controller_Name + "," + routModel.controller_Class );        
                    
                    
            //</editor-fold>
            
            //<editor-fold desc="Despatch">
            JSession.Request = request;
            JSession.Response=response;
            JSession.errorContainer = new ErrorContainer();
            JSession.errorContainer.errorList = new ArrayList<>();
            Class cls = Class.forName(routModel.controller_Class);
            Object obj = cls.newInstance();
            
            Result result =null;
            List<Object> resultObj = new ArrayList<>();
            Method[] declaredMethods = cls.getDeclaredMethods();
            
            for (Method declaredMethod : declaredMethods) {
               if (declaredMethod.getName().equals(action)) {
                    Parameter[] parameters = declaredMethod.getParameters();
                    //<editor-fold desc="loop parameter">
                    for (Parameter parameter : parameters) {
                        Annotation[] annotations = parameter.getDeclaredAnnotations();
                        Type paramType= parameter.getType();
                        String paramClsName =paramType.getTypeName();
                        Class paramCls;
                        Enum paramEnum;
                        Object paramObj =null;
                        for(Annotation annotation : annotations){
                            if(annotation instanceof jack.annotation.Param){
                                jack.annotation.Param paramAnnotation = (jack.annotation.Param) annotation;
                                if(parameter.getType().isPrimitive() 
                                        || paramType.getTypeName().equals(String.class.getTypeName())
                                        || paramType.getTypeName().equals(Date.class.getTypeName())
                                        ){
                                    paramObj = JSession.getRequest().getParameter(paramAnnotation.name());
                                    boolean nullAble = paramAnnotation.nullAble();
                                    String strParamValue = "";
                                    if(paramObj != null){
                                        strParamValue = paramObj.toString();
                                    }
                                    //<editor-fold desc="mapping primitive type">
                                    Object value = null;
                                    if(paramType.getTypeName().equals(String.class.getTypeName())){
                                        value = strParamValue;
                                    }else if(paramType.getTypeName().equals(int.class.getTypeName())){
                                        if(nullAble == false && JString.IsNullOrEmty(strParamValue)){
                                            throw new Exception("Require Parameter int:" + paramAnnotation.name());
                                        }
                                        if(!JString.IsNullOrEmty(strParamValue)){
                                            value = Integer.parseInt(strParamValue);
                                        }else{
                                            value = null;
                                        }
                                    }else if(paramType.getTypeName().equals(double.class.getTypeName())){
                                        if(JString.IsNullOrEmty(strParamValue)){
                                            throw new Exception("Require Parameter double:" + paramAnnotation.name());
                                        }
                                        value = Double.parseDouble(strParamValue);
                                    }else if(paramType.getTypeName().equals(Boolean.class.getTypeName())){
                                        if(JString.IsNullOrEmty(strParamValue)){
                                            value = Boolean.FALSE;
                                        }else{
                                            value = Boolean.parseBoolean(strParamValue);
                                        }
                                    }else if(paramType.getTypeName().equals(Date.class.getTypeName())){
                                        if(nullAble == false && JString.IsNullOrEmty(strParamValue)){
                                            throw new Exception("Require Parameter Date:" + paramAnnotation.name());
                                        }
                                        if(!JString.IsNullOrEmty(strParamValue)){
                                            try {
                                                value = JDate.StrConverToDateUTC(strParamValue);
                                            } catch (IllegalAccessException ex) {
                                                Logger.getLogger(CHelper.class.getName()).log(Level.SEVERE, null, ex);
                                                throw ex;
                                            }
                                        }else{
                                            value = null;
                                        }
                                    }
                                    //</editor-fold>
                                    resultObj.add(value);
                                }else if(paramType instanceof Class && ((Class<?>)paramType).isEnum()){
                                    paramCls = Class.forName(paramClsName);
                                    if(paramType instanceof Class && ((Class<?>)paramType).isEnum()){
                                        //Enum paramEnum;
                                    }else{
                                        paramObj = paramCls.newInstance();
                                    }
                                    //paramObj = GSession.getRequest().getParameter(paramAnnotation.name());
                                    String valueOfEnum = "";
                                    //<editor-fold desc="Get Enum of value string from httpRequest"> 
                                    try{
                                        valueOfEnum = JSession.getRequest().getParameter(paramAnnotation.name());
                                    }catch(Exception e){
                                        throw e;
                                    }   
                                    //</editor-fold>
                                    if(!JString.IsNullOrEmty(valueOfEnum)){
                                        paramEnum=Enum.valueOf(paramCls,valueOfEnum);
                                    }else{
                                        paramEnum=null;
                                    }
                                    resultObj.add(paramEnum);
                                }else{
                                    paramObj = MappingRequestToObj(paramObj,paramAnnotation.name(),JSession.getRequest());
                                    resultObj.add(paramObj);
                                }

                                System.out.println("name: " + paramAnnotation.name());
                            }
                        }
                         //Type type =parameter.getParameterizedType();
                        //String parameterName = parameter.getName();
                        // CHelper.MappingRequest(parameter, request);
                    }
                    //end for (Parameter parameter : parameters) {
                    //</editor-fold>
                    //paramObj = MappingRequestToObj(paramObj,paramAnnotation.name(),GSession.getRequest());
                    result = (Result) declaredMethod.invoke(obj,resultObj.toArray());    
                    //result = (Result) declaredMethod.invoke(obj,resultObj);
               break;
               }
            }
            if(result == null){
                throw new Exception("result not null");
            }
            //</editor-fold>
            
            //<editor-fold desc="Respone">
            if(JSession.errorContainer != null && JSession.errorContainer.errorList.size() > 0){
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                ErrorModel errorModel = JSession.getErrorContainer().errorList.stream().findFirst().get();
                String msg = errorModel.errorKey + ":" + errorModel.errorMsg;
                out.print(msg);
            }else{
                if(null != result.getResultType())switch (result.getResultType()) {
                    case view:
                        response.setContentType("text/html;charset=UTF-8");
                        View v;
                        v =(View) result;
                        jack.web.CHelper.View(v.getView_name(), request, response, v.getModel());
                        break;
                    case content:{
                        response.setContentType("text/html;charset=UTF-8");
                        Content c;
                        c =(Content) result;
                        PrintWriter out = response.getWriter();
                        out.print(c.getContentString());
                            break;
                        }
                    case json:{
                        response.setContentType("application/json;charset=UTF-8");
                        PrintWriter out = response.getWriter();
                        Json j;
                        j =(Json) result;
                        Gson gson = new Gson();
                        String json = gson.toJson(j.getObj());
                        out.write(json);
                            break;
                        }
                    case file:
                        break;
                    default:
                        break;
                }
            }
            //</editor-fold>
        }
        catch(Exception e )
        {
            //e.printStackTrace();
            PrintWriter out = response.getWriter();
            out.print(new jack.exception.JException().GetFullMessage(e));
        }
    }
    
    private void doDespatch1(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        JSession.Request = request;
        JSession.Response=response;
        JSession.errorContainer = new ErrorContainer();
        JSession.errorContainer.errorList = new ArrayList<ErrorModel>();
        //load the AppTest at runtime
        //no paramater
        Class noparams[] = {};
        //String parameter
        Class[] paramString = new Class[1];	
        paramString[0] = String.class;
        //int parameter
        Class[] paramInt = new Class[1];
        paramInt[0] = Integer.TYPE;
        Class[] paramClasses = new Class[2];
        paramClasses[0] = HttpServletRequest.class;
        paramClasses[1] = HttpServletResponse.class;
        try{
                //String requestURL = request.getRequestURI();
                String action = request.getParameter("[ac]");
                if(JString.IsNullOrEmty(action))
                {
                    throw new Exception("No parameter action [ac]");
                }
                //String[] parts = string.split("");
                String clientClassName = this.getClass().getName();
                Class cls = Class.forName(clientClassName);
                Object obj = cls.newInstance();
                //no paramater
                //call the printIt method
                //Method method = cls.getDeclaredMethod(action,paramClasses);
                Result result =null;
                List<Object> resultObj = new ArrayList<>();
                Method[] declaredMethods = cls.getDeclaredMethods();
                for (Method declaredMethod : declaredMethods) {
                    if (declaredMethod.getName().equals(action)) {
                        Parameter[] parameters = declaredMethod.getParameters();
                        //Object[] parameterObjs;
                        for (Parameter parameter : parameters) {
                            Annotation[] annotations = parameter.getDeclaredAnnotations();
                            Type paramType= parameter.getType();
                            String paramClsName =paramType.getTypeName();
                            Class paramCls = Class.forName(paramClsName);
                            Object paramObj =null;
                            Enum paramEnum;
                            if(paramType instanceof Class && ((Class<?>)paramType).isEnum()){
                                //Enum paramEnum;
                            }else{
                                paramObj = paramCls.newInstance();
                            }
                            for(Annotation annotation : annotations){
                                if(annotation instanceof jack.annotation.Param){
                                    jack.annotation.Param paramAnnotation = (jack.annotation.Param) annotation;
                                    if(paramType.getTypeName().equals(String.class.getTypeName())){
                                        paramObj = JSession.getRequest().getParameter(paramAnnotation.name());
                                        resultObj.add(paramObj);
                                    }else if(paramType instanceof Class && ((Class<?>)paramType).isEnum()){
                                        //paramObj = GSession.getRequest().getParameter(paramAnnotation.name());
                                        String valueOfEnum = "";
                                        //<editor-fold desc="Get Enum of value string from httpRequest"> 
                                        try{
                                            valueOfEnum = JSession.getRequest().getParameter(paramAnnotation.name());
                                        }catch(Exception e){
                                            throw e;
                                        }   
                                        //</editor-fold>
                                        if(!JString.IsNullOrEmty(valueOfEnum)){
                                            paramEnum=Enum.valueOf(paramCls,valueOfEnum);
                                        }else{
                                            paramEnum=null;
                                        }
                                        resultObj.add(paramEnum);
                                    }else{
                                        paramObj = MappingRequestToObj(paramObj,paramAnnotation.name(),JSession.getRequest());
                                        resultObj.add(paramObj);
                                    }
                                    
                                    System.out.println("name: " + paramAnnotation.name());
                                }
                            }
                             //Type type =parameter.getParameterizedType();
                            //String parameterName = parameter.getName();
                            // CHelper.MappingRequest(parameter, request);
                        }
                        //Object[] ObjecArr = 
                        result = (Result) declaredMethod.invoke(obj,resultObj.toArray());
                        
                        //result = (Result) declaredMethod.invoke(obj,resultObj);
                        break;
                    }
                }
                if(result == null){
                    throw new Exception("result not null");
                }
                
                //Class<?>[] param = method.getParameterTypes();
                //Result result = (Result) method.invoke(obj,request,response);
                if(JSession.errorContainer != null && JSession.errorContainer.errorList.size() > 0)
                {
                    response.setContentType("text/html;charset=UTF-8");
                    PrintWriter out = response.getWriter();
                    ErrorModel errorModel = JSession.getErrorContainer().errorList.stream().findFirst().get();
                    String msg = errorModel.errorKey + ":" + errorModel.errorMsg;
                    out.print(msg);
                }else{
                    if(result.getResultType() == ResultType.view){
                        response.setContentType("text/html;charset=UTF-8");
                        View v;
                        v =(View) result;
                        jack.web.CHelper.View(v.getView_name(), request, response, v.getModel());
                    }else if(result.getResultType() == ResultType.content){
                        response.setContentType("text/html;charset=UTF-8");
                        Content c;
                        c =(Content) result;
                        PrintWriter out = response.getWriter();
                        out.print(c.getContentString());
                    }else if(result.getResultType() == ResultType.json){
                        response.setContentType("application/json;charset=UTF-8");
                        PrintWriter out = response.getWriter();
                        Json j;
                        j =(Json) result;
                        Gson gson = new Gson();
                        String json = gson.toJson(j.getObj());
                        out.write(json);
                    }else if(result.getResultType() == ResultType.file){

                    }
                }
                //processRequest(request, response);
        }
        catch(Exception e)
        {
            //e.printStackTrace();
            PrintWriter out = response.getWriter();
            out.print(new jack.exception.JException().GetFullMessage(e));
        }
    }
    
    private void doDespatch3(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException, Exception {
        ErrorContainer errorContainer = new ErrorContainer();
        ErrorModel errorModel = new ErrorModel();
        
        //response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            JSession.Request = request;
            JSession.Response=response;
            String path = request.getPathInfo().substring(1);
            String[] paths = path.split("/");
            //out.print(path + paths.length);    
            if(paths.length != 2 || paths.length == 0){
                errorModel.errorMsg = "Require Controller and Action";
                errorContainer.errorList.add(errorModel);
                throw new JException(errorContainer);
            }else{
                String controllerName = paths[0];
                String actionName = paths[1];
                Class clazz = this.GetControllerClass(controllerName,request);
                Object clazzObj = clazz.newInstance();
                Result result = null;
                List<Object> paramObjList = new ArrayList<>();
                Method actionMethod = this.GetAction(clazz, actionName);
                Parameter[] parameters = actionMethod.getParameters();
                this.MappingParameter(parameters, paramObjList);
                result = (Result) actionMethod.invoke(clazzObj,paramObjList.toArray()); 
                if(result == null){
                    throw new Exception("result not null");
                }
                this.Render(result, request, response);
            }
            
        }
        catch(JException je) {
            DisplayError(je);
        }
        catch(IOException | ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException | ServletException e){
            //e.printStackTrace();
            DisplayError(e);
        }
    }
    
    

}


