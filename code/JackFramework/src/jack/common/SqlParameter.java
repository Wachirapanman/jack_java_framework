/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.common;

import java.util.Date;

/**
 *
 * @author Java
 */
public class SqlParameter {
    public Integer index;
    public String parameterName;
    public Object value;
    public DataType dataType;
    
    public SqlParameter(){
    }
    
    public SqlParameter(String parameterName,Object value,DataType dataType){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = dataType;
    }
    
    /***
     * 1:
     * @param parameterName
     * @param value 
     */
    public void SetString(String parameterName,String value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeString;
    }
    /***
     * 2:
     * @param parameterName
     * @param value 
     */
    public void SetInt(String parameterName,Integer value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeInt;
    }
    /***
     * 3:
     * @param parameterName
     * @param value 
     */
    public void setDouble(String parameterName,Double value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeDouble;
    }
    /***
     * 4:
     * @param parameterName
     * @param value 
     */
    public void setByte(String parameterName,Byte value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeByte;
    }
    /***
     * 5:
     * @param parameterName
     * @param value 
     */
    public void setDate(String parameterName,Date value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeDate;
    }
    /***
     * 6:
     * @param parameterName
     * @param value 
     */
    public void setDateTime(String parameterName,Date value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeDateTime;
    }
    /***
     * 7:
     * @param parameterName
     * @param value 
     */
    public void setFloat(String parameterName,Float value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeFloat;
    }
    /***
     * 8:
     * @param parameterName
     * @param value 
     */
    public void setLong(String parameterName,Long value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeLong;
    }
    /***
     * 9:
     * @param parameterName
     * @param value 
     */
    public void setBoolean(String parameterName,Boolean value){
        this.parameterName = parameterName;
        this.value = value;
        this.dataType = DataType.typeBoolean;
    }
    
    
    
}
