<%-- 
    Document   : index
    Created on : Dec 11, 2017, 3:16:00 AM
    Author     : Java
--%>

<%@page import="jack.web.el.Jel"%>
<jsp:include page="/view/share/header.jsp" />
<jsp:useBean id="Model" class="demo.web.viewModel.RoomViewModel"  scope="session"/> 
<!--room_-->
<div id="room_screen" style="padding:10px;">
    <div>Room Info</div>
    <table style="padding:10px;">
        <tr>
            <td>
                Room ID: <input type="text">
            </td>
            <td>
                Price: <input type="text">
            </td>
            <td>
                <button type="button" class="btn btn-primary" id="room_btnSearch">Search</button>
            </td>
        </tr>    
    </table>
    <div id="room_list"></div>
</div>
             
<script type="text/javascript">
    var room_var = {
        
    };
    var room_service = {
        loadList:function(){
            $("#room_list").load("room?[action]=list", function() {
                
            });
        }
    };
    
    var room_main = {
        load:function (){
            room_service.loadList();
            this.assignEvent();
        },
        assignEvent:function(){
            $("#room_btnSearch").click(function(){
                room_service.loadList();
            });
        }
    };
    $(document).ready(function() {
        room_main.load();
    });

</script>
