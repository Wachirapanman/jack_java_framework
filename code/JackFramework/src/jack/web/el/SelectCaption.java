/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.web.el;

/**
 *
 * @author Java
 */
public enum SelectCaption {
    none(1){
        @Override
        public String caption() {
            return "none";
        }
    },
    selectAll(2){
        @Override
        public String caption() {
            return "select all";
        }
    },
    pleaseSelect(3){
        @Override
        public String caption() {
            return "please select";
        }
    };
    
    private int value;
    public abstract String caption();
    
    private SelectCaption(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
    
    public static SelectCaption fromValue(int value) {  
        for (SelectCaption my: SelectCaption.values()) {  
            if (my.value == value) {  
                return my;  
            }  
        }  
        return null;  
    }  
}
