/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.common;

import jack.annotation.validate.NotNullValidator;
import jack.annotation.validate.NotNullValidator;
import jack.exception.ErrorContainer;
import jack.exception.ErrorModel;
import jack.exception.JValidateExeption;
import jack.lang.JString;
import jack.model.FieldValidateModel;
import jack.model.UserSession;
import jack.resources.Alert;
import jack.resources.ResourceManagment;
import java.lang.reflect.Field;
import java.util.AbstractList;
import java.util.ArrayList;

/**
 *
 * @author Java
 */
public class Validation {
    public static <T> T ValidateModel(T model) throws IllegalArgumentException, IllegalAccessException, InstantiationException, JValidateExeption{
        boolean result = false;
        jack.exception.ErrorContainer errorContainer = new ErrorContainer();
        errorContainer.errorList = new ArrayList<ErrorModel>();
        
        Class klazz = model.getClass();
        //ดู field ทั้งหมดที่ประกาศ
        for (Field f : klazz.getDeclaredFields()) {
            //<editor-fold desc="validate null object">
            boolean isNull = false;
            NotNullValidator notNullValidator = f.getAnnotation(NotNullValidator.class);
            if (notNullValidator != null){
                Object value = null;
                try{
                    value =f.get(model);
                }catch(Exception ex){
                    value = null;
                }
                //ถ้าเป็น string
                if(f.getType().getTypeName().equals(String.class.getTypeName())){
                    String strValue = (String) value;
                    if(JString.IsNullOrEmty(strValue)){
                        isNull = true;
                    }
                }else{
                    if(value == null){
                        isNull = true;
                    }
                }
                if(isNull == true){
                    jack.resources.Alert alertEnum = Alert.valueOf(notNullValidator.ResourceName());
                    String errorMsg =alertEnum.getString();
                     String tagMsg ="";
                    if(!JString.IsNullOrEmty( notNullValidator.BaseResource())){
                        tagMsg = ResourceManagment.getMessage(notNullValidator.BaseResource(),notNullValidator.Tag());
                    }else{
                        tagMsg = notNullValidator.Tag();
                    }
                    
                    errorMsg = errorMsg + " ["+ tagMsg + "]";
                    System.out.println("anno_name:" + notNullValidator.ResourceName()+",field_name:"+f.getName() + ",field_value:"+errorMsg);
                    jack.exception.ErrorModel errorModel = new ErrorModel();
                    errorModel.errorID = "valdate model";
                    errorModel.errorKey = "";
                    errorModel.errorMsg = errorMsg;
                    errorContainer.errorList.add(errorModel);
                }
            }
            //</editor-fold>
        }
        if(errorContainer.errorList.size() > 0){
            throw new jack.exception.JValidateExeption(errorContainer);
        }
        return model;
    }
    
    public static<T>  jack.model.FieldValidateModel getFieldValidateModel(T model,String fieldName) throws Exception{
        try{
             jack.model.FieldValidateModel result = new FieldValidateModel();
             Class klazz = model.getClass();
             Field filed;
             try{
                  filed = klazz.getField(fieldName);
             }catch(NoSuchFieldException | SecurityException e){
                 result= null;
                 return result;
             }
             result.filed = filed;
             NotNullValidator notNullValidator = null;
             try{
                notNullValidator = filed.getAnnotation(NotNullValidator.class);
             }catch(Exception e){
                 //not to do;
             }
             if (notNullValidator != null){
                 result.has_NotNullValidator = true;
             }
             return result;
        }catch(Exception ex){
            throw ex;
        }
    }
}
