<%-- 
    Document   : index
    Created on : Dec 20, 2017, 12:44:13 AM
    Author     : Java
--%>

<%@page import="demo.model.dataModel.Customer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="Model" class="demo.web.viewModel.CustomerViewModel"  scope="session"/> 
        <title></title>
    </head>
    <body>
        <table>
            <% for(Customer item : Model.customerList){  %>
            <tr>
                <td>
                    <%=item.code%>
                </td>
            </tr>
            <%}%>
        </table>

    </body>
</html>
<script type="text/javascript">
    var customerList_var = {
        
    };
    var customerList_service = {
        
    };
    
    var customerList_main = {
        load:function (){
            customerList_service.loadList();
            this.assignEvent();
        },
        assignEvent:function(){

        }
    };
    $(document).ready(function() {
        customerList_main.load();
    });

</script>
   