/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.common;

/**
 *
 * @author Java
 */
public enum DataType {
    typeString(1){
        @Override
        public String caption() {
            return "string";
        }
    },
    typeInt(2){
        @Override
        public String caption() {
            return "int";
        }
    },
    typeDouble(3){
        @Override
        public String caption() {
            return "double";
        }
    },
    typeByte(4){
        @Override
        public String caption() {
            return "byte";
        }
    },
    typeDate(5){
        @Override
        public String caption() {
            return "date";
        }
    },
    typeDateTime(6){
        @Override
        public String caption() {
            return "dateTime";
        }
    },
    typeFloat(7){
        @Override
        public String caption() {
            return "float";
        }
    },
    typeLong(8){
        @Override
        public String caption() {
            return "long";
        }
    },
    typeBoolean(9){
        @Override
        public String caption() {
            return "boolean";
        }
    };
    
    private int value;
    public abstract String caption();
    private DataType(int value) {
        this.value = value;
    }
}
