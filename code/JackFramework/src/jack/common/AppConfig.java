/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.common;

import jack.model.AppSetting;
import jack.model.DBConfig;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Java
 */
public class AppConfig {
    
    public DBConfig getDBConnection(String conStringName) throws Exception{
        try{
            // URL returned "/C:/Program%20Files/Tomcat%206.0/webapps/myapp/WEB-INF/classes/"
            URL path = this.getClass().getClassLoader().getResource("config.xml");
            // path decoded "/C:/Program Files/Tomcat 6.0/webapps/myapp/WEB-INF/classes/"
            String decoded = URLDecoder.decode(path.getFile(), "UTF-8");
//            //for windows     
//            if (decoded.startsWith("/")) {
//            // path "C:/Program Files/Tomcat 6.0/webapps/myapp/WEB-INF/classes/"
//                decoded = decoded.replaceFirst("/", "");
//            }
            File fXmlFile = new File(decoded);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            Element dbElement = (Element) doc.getElementsByTagName("database-config").item(0);
            NodeList nodeList = dbElement.getElementsByTagName("add");
            DBConfig dbConfig = new DBConfig();
            for(int i = 0; i < nodeList.getLength(); i++){
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    if(conStringName.equals(eElement.getAttribute("name"))){
                        dbConfig.db_name = eElement.getAttribute("db_name");
                        dbConfig.db_password = eElement.getAttribute("password");
                        dbConfig.db_url = eElement.getAttribute("url");
                        dbConfig.db_user = eElement.getAttribute("user");
                        dbConfig.db_driver = eElement.getAttribute("driver"); 
                    }
                }
            }
            return dbConfig;
        }catch(IOException | ParserConfigurationException | SAXException e){
            throw e;
        }
    }
    
    public  DBConfig getDatabaseConfig() throws IOException{
        Properties prop = new Properties();
        try {
            URL url = this.getClass().getClassLoader().getResource("app.config.properties");
            InputStream inputStream = new AppConfig().getClass().getResourceAsStream("app.config.properties");
            prop.load(inputStream);
            DBConfig dbConfig = new DBConfig();
            dbConfig.db_name = prop.getProperty("db_name");
            dbConfig.db_password = prop.getProperty("db_password");
            dbConfig.db_url = prop.getProperty("db_url");
            dbConfig.db_user = prop.getProperty("db_user");  
            dbConfig.db_driver = prop.getProperty("db_driver");  
          return dbConfig;  
        } catch (IOException e) {
            throw new IOException (e);
        }
    }
    
    public List<AppSetting> getAppSetting() throws IOException, ParserConfigurationException, SAXException{
        try{
            List<AppSetting> appSettingList = new ArrayList<>();
            // URL returned "/C:/Program%20Files/Tomcat%206.0/webapps/myapp/WEB-INF/classes/"
            URL path = this.getClass().getClassLoader().getResource("config.xml");
            // path decoded "/C:/Program Files/Tomcat 6.0/webapps/myapp/WEB-INF/classes/"
            String decoded = URLDecoder.decode(path.getFile(), "UTF-8");

            File fXmlFile = new File(decoded);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            Element settingElement = (Element) doc.getElementsByTagName("app-setting").item(0);
            NodeList nodeList = settingElement.getElementsByTagName("add");
            AppSetting appSetting;
            for(int i = 0; i < nodeList.getLength(); i++){
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    appSetting = new AppSetting();
                    appSetting.key = eElement.getAttribute("key");
                    appSetting.value = eElement.getAttribute("value");
                    appSettingList.add(appSetting);
                }
            }    
            return appSettingList;
        }catch(IOException | ParserConfigurationException | SAXException e){
            throw e;
        }
    }
    
    public AppSetting getAppSetting(String appStringName) throws IOException, Exception{
        try{
            AppSetting appSetting = new AppSetting();
            appSetting = new AppConfig().getAppSetting().stream().filter(x->x.key.equals(appStringName)).findFirst().orElse(null);
            return appSetting;
        }catch(IOException | ParserConfigurationException | SAXException e){
            throw e;
        }
    }
}
