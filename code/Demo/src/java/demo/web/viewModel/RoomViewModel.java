/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.web.viewModel;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Java
 */
public class RoomViewModel {
    public RoomViewModel(){
        dataList = new ArrayList();
    }
    public int roomID;
    public String name;
    public Date bookDate;
    public List<demo.model.dataModel.Room> dataList;
}
