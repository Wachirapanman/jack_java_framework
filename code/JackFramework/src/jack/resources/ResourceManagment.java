/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jack.resources;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author java
 */
public class ResourceManagment {
    public static String getMessage(String base,String tag) {
        try{
            ResourceBundle bundle =
            ResourceBundle.getBundle(base, Locale.getDefault());
            return bundle.getString(tag);
        }catch(Exception ex){
            return ex.getMessage();
        }
    }
}
