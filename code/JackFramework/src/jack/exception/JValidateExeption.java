/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.exception;

import java.util.ArrayList;

/**
 *
 * @author Java
 */
public class JValidateExeption extends Exception {
    public ErrorContainer errorContainer;
    public JValidateExeption(){
        errorContainer  = new ErrorContainer();
        errorContainer.errorList = new ArrayList<>();
    }
    public JValidateExeption(ErrorContainer error){
        errorContainer = error;
    }
}
