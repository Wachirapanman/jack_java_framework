/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jack.common;

import jack.annotation.Column;
import jack.lang.JDate;
import jack.lang.JString;
import jack.model.DBConfig;
import jack.model.SqlCondition;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Collectors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Java
 */
public class DBHelper {
    
    public static Connection getConnection(String conStringName) throws SQLException, IOException, ParserConfigurationException, SAXException, ClassNotFoundException, Exception{
        try{
            Connection con = null;
            DBConfig dbConfig = new AppConfig().getDBConnection(conStringName);
            Class.forName (dbConfig.db_driver);
            con = DriverManager.getConnection(dbConfig.db_url,dbConfig.db_user,dbConfig.db_password);
            return con;
        }
        catch(IOException | ClassNotFoundException | SQLException e)
        {
            throw e;
        }
    }
    
    public static ResultSet executeQuery(String sqlCommand,List<SqlParameter> sqlParameter) throws Exception{
        try{
            ResultSet rs = null;
                
            return rs;
        }catch(Exception e){
            throw new Exception(e);
        }
    }
    
    public static PreparedStatement setPreparedStatement(Connection conn,String sqlCommand,List<SqlParameter> sqlParameterList) throws Exception{
        try{
            PreparedStatement pstmt = null;
            String sql = sqlCommand;
            String sqltemp = sqlCommand;
            int index = 0;
            for(SqlParameter sqlParameter : sqlParameterList){
                sql = sql.replace(sqlParameter.parameterName,"?");
                index=sqltemp.indexOf(sqlParameter.parameterName);
                sqlParameter.index = index;
            }
            sqlParameterList = sqlParameterList.stream().sorted((s1,s2) -> s1.index.compareTo(s2.index)).collect(Collectors.toList());
            //set PreparedStatement
            //check index out of range
            
            int size = JString.SearchCount("?",sql);
            if(size != sqlParameterList.size()){
                throw new Exception("DBHelper.setPreparedStatement : sql index out of range @field not match");
            }
            
            pstmt = conn.prepareStatement(sql); // create a statement
            int i = 0;
            for(SqlParameter sqlParameter : sqlParameterList){
                i++;
                if(sqlParameter.dataType == DataType.typeString){
                    String value = "";
                    if(sqlParameter.value == null){
                        pstmt.setNull(i,java.sql.Types.VARCHAR);
                    }else{
                        value = sqlParameter.value.toString();
                        pstmt.setString(i,value);
                    }
                }else if(sqlParameter.dataType == DataType.typeInt){
                    if(sqlParameter.value == null){
                        pstmt.setNull(i,java.sql.Types.INTEGER);
                    }else{
                        pstmt.setInt(i,(int) sqlParameter.value);
                    }
                }else if(sqlParameter.dataType == DataType.typeDouble){
                    if(sqlParameter.value == null){
                        pstmt.setNull(i,java.sql.Types.DOUBLE);
                    }else{
                        pstmt.setDouble(i,(double) sqlParameter.value);
                    }
                }else if(sqlParameter.dataType == DataType.typeByte){
                    pstmt.setByte(i,(byte) sqlParameter.value);
                }else if(sqlParameter.dataType == DataType.typeFloat){
                    try{ 
                        if(sqlParameter.value == null){
                            pstmt.setNull(i,java.sql.Types.FLOAT);
                        }else{
                            pstmt.setFloat(i,(float) sqlParameter.value);
                        }
                    }catch(SQLException ex){
                        throw ex;
                    }
                }else if(sqlParameter.dataType == DataType.typeDate){
                    if(sqlParameter.value == null){
                        pstmt.setNull(i,java.sql.Types.DATE);
                    }else{
                        Date dateTime1 = (Date) sqlParameter.value;
                        Date dateTime2 =JDate.DateConverToDateFormateUS(dateTime1);  
                        java.sql.Date sqlDate = new java.sql.Date(dateTime2.getTime()); 
                        pstmt.setDate(i,sqlDate,Calendar.getInstance(Locale.US));
                    }
                }else if(sqlParameter.dataType == DataType.typeDateTime){
                    try{
                        if(sqlParameter.value == null){
                            pstmt.setNull(i,java.sql.Types.TIMESTAMP);
                        }else{
                            Date dateTime1 = (Date) sqlParameter.value;
                            Date dateTime2 =JDate.DateTimeConverToDateTimeFormateUS(dateTime1);  
                            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(dateTime2.getTime());
                            pstmt.setTimestamp(i, sqlTimestamp, Calendar.getInstance(Locale.US));
                        }
                    }catch(SQLException ex){
                        throw ex;
                    }
                }else if(sqlParameter.dataType == DataType.typeLong){
                    if(sqlParameter.value == null){
                        pstmt.setNull(i,java.sql.Types.BIGINT);
                    }else{
                        pstmt.setLong(i,(long) sqlParameter.value);
                    }
                }else if(sqlParameter.dataType == DataType.typeBoolean){
                    if(sqlParameter.value == null){
                        pstmt.setBoolean(i,false);
                    }else{
                        pstmt.setBoolean(i,(boolean) sqlParameter.value);
                    }
                }
            }
            
            return pstmt;
        }catch(SQLException e){
            throw e;
        }
    }
    
    public static <T> List<T> MappingModel(ResultSet rs,List<T> modelList,T model) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        try{
            //1.loop rs แต่ละ line
            Class klazz = model.getClass();
            //String[] parts = string.split("");
            String clientClassName = model.getClass().getName();
            Class cls = Class.forName(clientClassName);
            
            ResultSetMetaData meta = rs.getMetaData();
            final int columnCount = meta.getColumnCount();
            meta.getColumnName(columnCount);
            while(rs.next()){ 
                //loop ทุก filed เพื่อ mapping กับ rs
                T data = (T) cls.newInstance();
                for (Field field : data.getClass().getDeclaredFields()) {
                    //get annotation
                    Column column = field.getAnnotation(Column.class);
                    if (column != null && !JString.IsNullOrEmty(column.name())){
                        //if rs == column name mapping data
                        rs.getObject(column.name());
                        
                    }
                }
               
//               model.domainID = rs.getString("domainID");
//               model.userID = rs.getString("userID");
//               model.userRoleID = rs.getString("userRoleID");
//               model.name = rs.getString("name");
//               model.firstName = rs.getString("firstName");
//               model.lastName = rs.getString("lastName");
//               model.birthDay = rs.getDate("birthDay");
//               model.phoneNumber = rs.getString("phoneNumber");
//               model.email = rs.getString("email");
//               model.gender = Gender.fromValue(rs.getInt("gender"));
//               model.address = rs.getString("address");
//               model.isActive = rs.getBoolean("isActive");
//               model.createBy = rs.getString("createBy");
//               model.createDate = rs.getTimestamp("createDate");
//               model.updateBy = rs.getString("updateBy");
//               model.updateDate = rs.getTimestamp("updateDate");

            }
            
            //ดู field ทั้งหมดที่ประกาศ
            for (Field f : klazz.getDeclaredFields()) {
                
            }
            return modelList;    
        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException | SecurityException e){
            throw e;
         
        }
    }
    
    public static <T> void setFilter(String sqlFilterCMD,List<SqlParameter> sqlParameterList
            ,T modelFilter,String columnName,SqlCondition sqlCondition) throws Exception{
        try{
            //หา field ของ modelfilter ว่ามีหรือไม่
            //1.loop rs แต่ละ line
            Field field  = modelFilter.getClass().getField(columnName);
            if(field != null){
                field.setAccessible(true); // You might want to set modifier to public first.
                Object value = field.get(modelFilter); 
                //ตรวจสอบ dataType field of modelFilter
                if(field.getType().getTypeName().equals(String.class.getTypeName()))
                {
                    String strValue = value.toString();
                    if(!JString.IsNullOrEmty(strValue)){
                        sqlFilterCMD += (!JString.IsNullOrEmty(sqlFilterCMD)) ? sqlCondition : "";
                        sqlFilterCMD += columnName +"="+ "@" +columnName;
                        SqlParameter sqlParameter = new  SqlParameter();
                        sqlParameter.SetString("@" +columnName,strValue);
                        sqlParameterList.add(sqlParameter);
                    }
                }
            }else{
                throw new Exception("Not have column :"+ columnName);
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public static Date rsGetTimestamp(ResultSet rs, String columnName) throws Exception{
        try{
            Date result = new Date();
            result = rs.getTimestamp(columnName,Calendar.getInstance(Locale.US));
            return result;
        }catch(Exception e){
            throw e;
        }
    }
}
